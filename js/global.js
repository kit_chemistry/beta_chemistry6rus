//SOUNDS
var audio = [],
	audioPiece = [];

//TIMEOUTS
var timeout = [],
	launch = [],
	sprites = [];

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum),
			preLoadImage = $("#pre-load .pre-load-image");
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			preLoadImage.css("width", loadPercentage + "%");
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

var soundToZero = function(audiofile, vol)
{
	if(vol >= 0)
	{
		audiofile.volume = vol/10;
		vol --;
		timeout[0] = setTimeout(function(){
			soundToZero(audiofile, vol);
		}, 1000);
	}
}	

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
}

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
	
	var currPiece = this,
		currAudio = this.audio;
	
	currAudio.addEventListener("timeupdate", function(){
		var currTime = Math.round(currAudio.currentTime);
		
		if(currTime == currPiece.end)
			currAudio.pause();
	});
}

AudioPiece.prototype.play = function()
{
	this.audio.currentTime = this.start;
	this.audio.play();
}

AudioPiece.prototype.pause = function()
{
	this.audio.pause();
}

AudioPiece.prototype.addEventListener = function(e, callBack)
{
	var currPiece = this,
		currAudio = this.audio,
		currDuration = Math.round(currAudio.duration);
	
	if(e === "ended")
	{
		console.log("else end: " + currPiece.end + ", " + currDuration);
		currAudio.addEventListener("timeupdate", function(){
			var currTime = Math.round(currAudio.currentTime);
				
			if(currTime === currPiece.end)
			{
				currAudio.pause();
				currAudio.currentTime += 3;
				callBack();
			}
		});
	}
	if(e === "timeupdate")
	{
		currPiece.audio.addEventListener("timeupdate", callBack);
	}
}

AudioPiece.prototype.getStart = function()
{
	return this.start;
}

launch["frame-000"] = function(){
		theFrame = $("#frame-000"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		menuRoll = $(prefix + ".menu-roll"),
		menuRollFinal = $(prefix + ".menu-roll-final"),
		chestOpen = $(prefix + ".chest-open"),
		chestClosed = $(prefix + ".chest-closed"),
		button = $(prefix + ".button");
	
	sprites[0] = new Motio(menuRoll[0], {
		fps: 4,
		frames: 10
	});
	
	sprites[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
		
	menuRollFinal.fadeOut(0);
	//menuRoll.fadeOut(0);
	bg2.fadeOut(0);
	chestOpen.fadeOut(0);
	chestClosed.fadeOut(0);
	
	audio[0] = new Audio("audio/main-theme.mp3");
	audio[1] = new Audio("audio/magic.mp3");
	audio[2] = new Audio("audio/chest-close.mp3");
	audio[3] = new Audio("audio/s1-1.mp3");
	
	var buttonListener = function(){
		audio[0].volume = 0.3;
		audio[0].play();
		button.fadeOut(1000);
	
		//chestCl.fadeOut(0);
		timeout[0] = setTimeout(function(){
			sprites[0].play();
		}, 2500);
		timeout[1] = setTimeout(function(){
			menuRoll.fadeOut(0);
			menuRollFinal.fadeIn(0);
		}, 4200);
		timeout[2] = setTimeout(function(){
			menuRollFinal.fadeOut(1000);
		}, 5000);
		timeout[2] = setTimeout(function(){
			chestOpen.fadeIn(1000);
		}, 7000);
		timeout[3] = setTimeout(function(){
			chestOpen.addClass("chest-open-big");
			bg1.fadeOut(1000);
			bg2.fadeIn(1000);
			audio[1].play();
		}, 8000);
		timeout[4] = setTimeout(function(){
			audio[3].play();
		}, 10000);
		timeout[5] = setTimeout(function(){
			chestOpen.fadeOut(0);
			chestClosed.fadeIn(0);
			audio[2].play();
		}, 13000);
		timeout[6] = setTimeout(function(){
			chestClosed.addClass("chest-closed-small");
		}, 16000);
		timeout[7] = setTimeout(function(){
			fadeNavsIn();
			soundToZero(audio[0], 3);
		}, 22000);
	};
	
	button.off("click", buttonListener);
	button.on("click", buttonListener);
	
	startButtonListener = buttonListener;
}

launch["frame-001"] = function(){
		theFrame = $("#frame-001"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		doorOpen = $(prefix + ".door-open"), 
		doorClosed = $(prefix + ".door-closed"),
		boy = $(prefix + ".boy"),
		lockH = $(prefix + ".lock-highlighted"), 
		methodLabel = $(prefix + ".method-label"),
		methodLabelH = $(prefix + ".method-label-highlighted"),
		bagsAndBoxes = $(prefix + ".bags-and-boxes"),
		bagsAndBoxesH = $(prefix + ".bags-and-boxes-highlighted"),
		boySprite = new Motio(boy[0], {
			fps: 5,
			frames: 2
		});
	
	lockH.fadeOut(0);
	methodLabelH.fadeOut(0);
	bagsAndBoxesH.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/s2-1.mp3");
	audio[1] = new Audio("audio/s3-1.mp3");
		
	doorOpen.fadeOut(0);
	
	var doneSecond = 0;	
	audio[1].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime);
		if(currTime === 1 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			lockH.fadeIn(500);
			timeout[1] = setTimeout(function(){
				lockH.fadeOut(0);
			}, 2000);
		}
		else if(currTime === 5 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			bagsAndBoxesH.fadeIn(500);
			boy.css("opacity", "0.2");
			timeout[2] = setTimeout(function(){
				bagsAndBoxesH.fadeOut(0);
				boy.css("opacity", "");
				methodLabelH.fadeIn(500);
			}, 2000);
		}
	});
	
	audio[0].addEventListener("ended", function(){
		audio[1].play();
	});
	
	audio[1].addEventListener("ended", function(){
		boySprite.pause();
		boySprite.to(0, true);
		timeout[3] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	});
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
	};
}

launch["frame-002"] = function(){
		theFrame = $("#frame-002"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		doorOpen = $(prefix + ".door-open"), 
		doorClosed = $(prefix + ".door-closed"),
		boy = $(prefix + ".boy"),
		methodLabel = $(prefix + ".method-label"),
		bagsAndBoxes = $(prefix + ".bags-and-boxes"),
		whiteCloud = $(prefix + ".white-cloud"), 
		apples = $(prefix + ".apples"),
		coins = $(prefix + ".coins"),
		candies = $(prefix + ".candies"),
		appleMolecule = $(prefix + ".apple-molecule");
	
	audio[0] = new Audio("audio/s3-2.mp3"),
	
	boySprite = new Motio(boy[0], {
		fps: 5,
		frames: 2
	});
	
	appleMoleculeSprite = new Motio(appleMolecule[0], {
		fps: 1,
		frames: 5
	});
	
	appleMoleculeSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	whiteCloud.fadeOut(0);
	coins.fadeOut(0);
	candies.fadeOut(0);
	apples.fadeOut(0);
	appleMolecule.fadeOut(0);
	fadeNavsIn();
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 5 && currTime !== doneSecond)
		{
			coins.fadeOut(500);
			candies.fadeOut(500);
			apples.animate({
				"background-size": "1500%"}, 4000, function(){
					appleMolecule.fadeIn(1000);
					appleMoleculeSprite.play();
				});
		}		
	});
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
		timeout[4] = setTimeout(function(){
			apples.fadeOut(500);
			whiteCloud.fadeOut(500);
			appleMolecule.fadeOut(500);
			boySprite.to(0, true);
			fadeNavsIn();
		}, 3000);
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play();
		
		timeout[1] = setTimeout(function(){
			whiteCloud.fadeIn(0);
			apples.fadeIn(500);
		}, 2000);
		timeout[2] = setTimeout(function(){
			candies.fadeIn(500);
		}, 3000);
		timeout[3] = setTimeout(function(){
			coins.fadeIn(500);
		}, 4000);
	};
}

launch["frame-003"] = function(){
		theFrame = $("#frame-003"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		woodLabel = $(prefix + ".wood-label"),
		woodExit = $(prefix + ".wood-exit"),
		doorOpen = $(prefix + ".door-open"), 
		doorClosed = $(prefix + ".door-closed"),
		boy = $(prefix + ".boy"),
		methodLabel = $(prefix + ".method-label"),
		methodLabelH = $(prefix + ".method-label-highlighted"),
		scales = $(prefix + ".scales"),
		scalesHighlighted = $(prefix + ".scales-highlighted"),
		bag1 = $(prefix + ".bag-1"),
		bag1Highlighted = $(prefix + ".bag-1-highlighted"),
		bag2 = $(prefix + ".bag-2"),
		bag2Highlighted = $(prefix + ".bag-2-highlighted"),
		key = $(prefix + ".key"),
		answerBags = $(prefix + ".answer-bag");
	
	audio[0] = new Audio("audio/s3-3.mp3");
	audio[3] = new Audio("audio/door-open.mp3");
	audio[4] = new Audio("audio/footsteps.mp3");
	
	var boySprite = new Motio(boy[0], {
		fps: 5,
		frames: 2
	}),
	scalesSprite = new Motio(scales[0], {
		fps: 4,
		frames: 5
	});
	
	woodExitSprite = new Motio(woodExit[0], {
		fps: 4, 
		frames: 8
	});
	
	scalesSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	woodExitSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			fadeNavsIn();
		}
	});
	
	fadeNavsIn();
	doorOpen.fadeOut(0);
	answerBags.fadeOut(0);
	key.fadeOut(0);
	woodExit.fadeOut(0);
	bag1Highlighted.fadeOut(0);
	bag2Highlighted.fadeOut(0);
	scalesHighlighted.fadeOut(0);
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 8 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			bag2.fadeOut(0);
			bag2Highlighted.fadeIn(0);
		}
		else if(currTime === 16 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			bag2.fadeIn(0);
			bag2Highlighted.fadeOut(0);
			scales.fadeOut(0);
			scalesHighlighted.fadeIn(0);
		}
		else if(currTime === 19 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			scales.fadeIn(0);
			scalesHighlighted.fadeOut(0);
		}
	});
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
	});
	
	audio[4].addEventListener("ended", function(){
		blackSkin.fadeIn(0);
		nextButton.fadeIn(0);
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play();
		timeout[0] = setTimeout(function(){
			bag1.fadeOut(0);
			bag1Highlighted.fadeIn(0);
			timeout[1] = setTimeout(function(){
				bag1.fadeIn(0);
				bag1Highlighted.fadeOut(0);
			}, 3000);
		}, 2000);
	}
	
	var answerBagListener = function(){
		if($(this).attr("data-correct"))
		{
			key.fadeIn(0);
			scales.fadeOut(0);
			key.addClass("transition-2s");
			key.css("top", "45%");
			key.css("left", "20%");
			timeout[4] = setTimeout(function(){
				doorClosed.fadeOut(0);
				doorOpen.fadeIn(0);
				key.fadeOut(500);
				audio[3].play();
				bag1.fadeOut(0);
				bag2.fadeOut(0);
				methodLabel.fadeOut(0);
				methodLabelH.fadeOut(0);
				woodLabel.fadeOut(0);
				answerBags.fadeOut(0);
				timeout[5] = setTimeout(function(){
					doorOpen.fadeOut(0);
					woodExit.fadeIn(0);
					woodExitSprite.play();
					audio[4].play();
				}, 2000);
			}, 2000);
		}
	}
	
	answerBags.off("click", answerBagListener);
	answerBags.on("click", answerBagListener);
	
	var draggableBag = $("#frame-003 .draggable-ball");
	draggabillyBag = new Draggabilly(draggableBag[0]);
	var vegetable, basket;
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target);
		vegetable.css("background-image", "url('pics/bag-2-highlighted.png')");
	};
	var onEnd = function(instance, event, pointer){
		vegetable.css("background-image", "");
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(basket.hasClass("scales-basket"))
		{
			scalesSprite.play();
			timeout[0] = setTimeout(function(){
				answerBags.fadeIn(0);
				boy.fadeOut(0);
			}, 2000);
		}
		else
		{
			vegetable.css("left", "");
			vegetable.css("top", "");
			vegetable.fadeIn(0);
		}
	};
	
	draggabillyBag.on("dragStart", onStart);
	draggabillyBag.on("dragEnd", onEnd);
}

launch["frame-004"] = function(){
		theFrame = $("#frame-004"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + ".boy"),
		doorClosed = $(prefix + ".door-closed"),
		doorOpen = $(prefix + ".door-open"),
		sandScheme = $(prefix + ".sand-scheme"),
		sioAnimated = $(prefix + ".sio-animated");
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	sioAnimatedSprite = new Motio(sioAnimated[0], {
		fps: 1,
		frames: 5
	});
	
	sioAnimatedSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
		}
	});
	
	sandScheme.fadeOut(0);	
	doorOpen.fadeOut(0);
	fadeNavsIn();
	sioAnimated.fadeOut(0);
	
	audio[0] = new Audio("audio/s4-1.mp3");
	audio[1] = new Audio("audio/s4-2.mp3");
	
	audio[0].addEventListener("ended", function(){
		audio[1].play();
		
		timeout[1] = setTimeout(function(){
			sandScheme.fadeOut(0);
		}, 5000);
		timeout[2] = setTimeout(function(){
			sioAnimated.fadeIn(500);
			sioAnimatedSprite.play();
		}, 20000);
	});
	
	audio[1].addEventListener("ended", function(){
		boySprite.pause();
		fadeNavsIn();
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play(0);
		timeout[0] = setTimeout(function(){
			sandScheme.fadeIn(500);
		}, 5000);
	}
}

launch["frame-005"] = function(){
		theFrame = $("#frame-005"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + ".boy"),
		doorClosed = $(prefix + ".door-closed"),
		doorOpen = $(prefix + ".door-open"),
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		taskLabel = $(prefix + ".task-label"),
		correct = $(prefix + ".correct"),
		keepThinking = $(prefix + ".keep-thinking"),
		left1 = $(prefix + ".left-1"), 
		left2 = $(prefix + ".left-2"),
		right = $(prefix + ".right"), 
		left1Label = left1.html(),
		left2Label = left2.html(),
		rightLabel = right.html(),
		arrows = $("#frame-005 .arrow, #frame-005 .arrow-stone"),
		cNum = 1, o2Num = 1, co2Num = 1;
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	correct.fadeOut(0);
	keepThinking.fadeOut(0);
	taskLabel.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/s4-3.mp3");
	audioPiece[0] = new AudioPiece("s4-3", 20, 25);
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 20 && currTime != doneSecond)
		{
			doneSecond = currTime;
			currAudio.pause();
			boySprite.pause();
			taskLabel.fadeIn(0);
		}
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play();
	}
	
	var arrowListener = function(){
		if(cNum === o2Num && o2Num === co2Num)
		{
			correct.fadeIn(0);
			timeout[0] = setTimeout(function(){
				correct.fadeOut(0);
				boySprite.play();
				audioPiece[0].play();
				fadeNavsIn();
				timeout[1] = setTimeout(function(){
					boySprite.pause();
				}, 4000);
			}, 2000);
		}
		else
		{
			keepThinking.fadeIn(0);
			timeout[0] = setTimeout(function(){
				keepThinking.fadeOut(0);
			}, 2000);
		}
	}
	
	arrows.off("click", arrowListener);
	arrows.on("click", arrowListener);
	
	var draggable = $("#frame-005 .elem"),
		draggabilly = [];
	for (var i = 0; i < draggable.length; i++)
	{
		draggabilly[i] = new Draggabilly(draggable[i]);
	}
	
	var vegetable, vegetableClone, basket;
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target);
		vegetableClone = vegetable.clone();
	};
	var onEnd = function(instance, event, pointer){
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(basket.attr("data-key") === vegetable.attr("data-key"))
		{
			if(basket.hasClass("elem"))
				basket = basket.parent();
			
			basket.append(vegetableClone);
			if(basket.hasClass("left-1-stone"))
			{
				cNum++;
				left1.html(cNum + left1Label);
			}
			if(basket.hasClass("left-2-stone"))
			{
				o2Num++;
				left2.html(o2Num + left2Label);
			}
			if(basket.hasClass("right-stone"))
			{
				co2Num++;
				right.html(co2Num + rightLabel);
			}
		}
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.fadeIn(0);
	};
	
	for(var i = 0; i < draggabilly.length; i++)
	{
		draggabilly[i].on("dragStart", onStart);
		draggabilly[i].on("dragEnd", onEnd);
	}
}

launch["frame-006"] = function(){
		theFrame = $("#frame-006"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + ".boy"),
		gramPerMole = $(prefix + ".gram-per-mole"),
		mole = $(prefix + ".mole"),
		doorClosed = $(prefix + ".door-closed"),
		doorOpen = $(prefix + ".door-open");
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	gramPerMole.fadeOut(0);
	mole.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/s4-41.mp3");
	
	audio[0].addEventListener("ended", function(){
		gramPerMole.fadeOut(500);
		mole.fadeIn(500);
		boySprite.pause();
		timeout[0] = setTimeout(function(){
			mole.fadeOut(500);
		}, 4000);
		timeout[1] = setTimeout(function(){
			fadeNavsIn();
		}, 5000);
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play();
		gramPerMole.fadeIn(500);
	}
}

launch["frame-007"] = function(){
		theFrame = $("#frame-007"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + ".boy"),
		doorClosed = $(prefix + ".door-closed"),
		doorOpen = $(prefix + ".door-open"),
		molarMassKz = $(prefix + ".molar-mass-kz"), 
		molarMassRu = $(prefix + ".molar-mass-ru"),
		molarMassEn = $(prefix + ".molar-mass-en"),
		gramPerMole = $(prefix + ".gram-per-mole");
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	gramPerMole.fadeOut(0);
	molarMassKz.fadeOut(0), 
	molarMassRu.fadeOut(0),
	molarMassEn.fadeOut(0);
	gramPerMole.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/s4-42.mp3");
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 7 && currTime != doneSecond)
		{
			doneSecond = currTime;
			molarMassKz.fadeIn(0);
		}
		else if(currTime === 8 && currTime != doneSecond)
		{
			doneSecond = currTime;
			molarMassRu.fadeIn(500);
		}
		else if(currTime === 9 && currTime != doneSecond)
		{
			doneSecond = currTime;
			molarMassEn.fadeIn(1000);
		}
		else if(currTime === 12 && currTime != doneSecond)
		{
			doneSecond = currTime;
			molarMassKz.fadeOut(0), 
			molarMassRu.fadeOut(0),
			molarMassEn.fadeOut(0);
			gramPerMole.fadeIn(2000);
			timeout[0] = setTimeout(function(){
				fadeNavsIn();
			}, 5000);
		}
	});
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play();
	}
}

launch["frame-008"] = function(){
		theFrame = $("#frame-008"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		allQuestionAnswers = $(prefix + " .task, #" + theFrame.attr("id") + " .answer"), 
		task1 = $(prefix + " .task-1"),
		task2 = $(prefix + " .task-2"),
		task3 = $(prefix + " .task-3"),
		task4 = $(prefix + " .task-4"),
		task5 = $(prefix + " .task-5"),
		answers = $(prefix + " .answer"),
		result = $(prefix + " .result"),
		activeQuestion = 1,
		correct = 0;
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	fadeNavsIn();
	allQuestionAnswers.fadeOut(0);
	result.fadeOut(0);
	
	audioPiece[0] = new AudioPiece("s4-5", 0, 4);
	
	audioPiece[0].addEventListener("ended", function(){
		audioPiece[0].pause();
		boySprite.pause();
		boy.fadeOut(1000);
		$(prefix + ".task-" + activeQuestion).fadeIn(500);
	});
	
	startButtonListener = function(){
		boySprite.play();
		audioPiece[0].play();
	}
	
	var answerListener = function(){
		if($(this).attr("data-correct"))
			correct ++;
		allQuestionAnswers.fadeOut(0);
		activeQuestion++;
		if(activeQuestion > 5)
		{
			result.html("Правильных <br>ответов: "+correct);
			result.fadeIn(0);
			timeout[0] = setTimeout(function(){
				result.fadeOut(0);
				fadeNavsIn();
			}, 3000);
		}
		else
			$(prefix + " .task-" + activeQuestion).fadeIn(0);	
	};
	
	answers.off("click", answerListener);
	answers.on("click", answerListener);
}

launch["frame-009"] = function(){
		theFrame = $("#frame-009"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		avogadroNumber = $(prefix + " .avogadro-number"),
		avogadroKaz = $(prefix + " .avogadro-kaz"),
		avogadroRus = $(prefix + " .avogadro-rus"),
		avogadroEng = $(prefix + " .avogadro-eng"),
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	avogadroNumber.fadeOut(0);
	avogadroKaz.fadeOut(0);
	avogadroRus.fadeOut(0);
	avogadroEng.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/s4-44.mp3");
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 6 && currTime != doneSecond)
		{
			doneSecond = currTime;
			avogadroNumber.fadeIn(500);
		}
		if(currTime === 16 && currTime != doneSecond)
		{
			doneSecond = currTime;
			avogadroNumber.fadeOut(0);
			avogadroKaz.fadeIn(500);
			timeout[0] = setTimeout(function(){
				avogadroRus.fadeIn(500);
				timeout[1] = setTimeout(function(){
					avogadroEng.fadeIn(500);
					timeout[2] = setTimeout(function(){
						fadeNavsIn();
					}, 3000);
				}, 1000);
			}, 1000);
		}
	});
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
	});
	
	startButtonListener = function(){
		audio[0].play();
		boySprite.play();
	}
}

launch["frame-010"] = function(){
		theFrame = $("#frame-010"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		startButton = $(prefix + " .start-button"),
		nextButton = $(prefix + " .next-button"),
		blackSkin = $(prefix + " .black-skin"),
		bucket1 = $(prefix + " .bucket-1"),
		bucket2 = $(prefix + " .bucket-2"),
		bucket3 = $(prefix + " .bucket-3"),
		bucket4 = $(prefix + " .bucket-4"),
		key = $(prefix + " .key"),
		sandExit = $(prefix + " .sand-exit");
	
	var boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	var sandExitSprite = new Motio(sandExit[0], {
		fps: 3,
		frames: 7
	});
	
	sandExitSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			fadeNavsIn();
		}
	});
	
	doorOpen.fadeOut(0);
	fadeNavsIn();
	key.fadeOut(0);
	sandExit.fadeOut(0);
	
	audio[0] = new Audio("audio/s4-5.mp3");
	audio[1] = new Audio("audio/door-open.mp3");
	audio[2] = new Audio("audio/stone-sound.mp3");
	audio[3] = new Audio("audio/footsteps.mp3");
	audioPiece[0] = new AudioPiece("s4-5", 32, 40)
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
		
		if(currTime === 19 && currTime != doneSecond)
		{
			doneSecond = currTime;
			boySprite.pause();
		}
		else if(currTime === 24 && currTime != doneSecond)
		{
			doneSecond = currTime;
			boySprite.play();
		}
		else if(currTime === 29 && currTime != doneSecond)
		{
			doneSecond = currTime;
			currAudio.pause();
			boySprite.pause();
			boy.fadeOut(500);
		}
	});
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
	}
	
	var draggable = $("#frame-010 .stone"),
		draggabilly = [];
	for (var i = 0; i < draggable.length; i++)
	{
		draggabilly[i] = new Draggabilly(draggable[i]);
	}
	
	var vegetable, vegetableClone, basket,
		bucket1Num = 0, bucket2Num = 0, bucket3Num = 0, bucket4Num = 0;
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target);
	};
	var onEnd = function(instance, event, pointer){
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(basket.attr("data-key") === vegetable.attr("data-key"))
		{
			switch(basket.attr("data-key"))
			{
				case "m1": bucket1Num++;
						   bucket1.css("background-image", "url(pics/bucket-label-1.png), url('pics/bucket-lvl-"+bucket1Num+".png')");
							break;
				case "m2": bucket2Num++;
							if(bucket2Num == 4) bucket2Num --;
							bucket2.css("background-image", "url(pics/bucket-label-2.png), url('pics/bucket-lvl-"+bucket2Num+".png')");
							break;
				case "m3": bucket3Num++;
							bucket3.css("background-image", "url(pics/bucket-label-3.png), url('pics/bucket-lvl-"+bucket3Num+".png')");
							break;
				case "m6": bucket4Num++;
							bucket4.css("background-image", "url(pics/bucket-label-4.png), url('pics/bucket-lvl-"+bucket4Num+".png')");
							break;
			}
			audio[2].play();
			vegetable.remove();
			if(!$(prefix + " .stone").length)
			{
				bucket1.fadeOut(0);
				bucket2.fadeOut(0);
				bucket3.fadeOut(0);
				bucket4.fadeOut(0);
				key.fadeIn(0);
				key.addClass("transition-2s");
				audioPiece[0].play();
				timeout[0] = setTimeout(function(){
					key.css("left", "10%");
					key.css("top", "45%");
					timeout[1] = setTimeout(function(){
						sandExit.fadeIn(0);
						audio[1].play();
						timeout[2] = setTimeout(function(){
							audio[3].play();
							sandExitSprite.play();
						}, 1000);
					}, 2000);
				}, 2000);
			}
		}
		else
		{
			vegetable.css("left", "");
			vegetable.css("top", "");
			vegetable.fadeIn(0);
		}
	};
	
	for(var i = 0; i < draggabilly.length; i++)
	{
		draggabilly[i].on("dragStart", onStart);
		draggabilly[i].on("dragEnd", onEnd);
	}
}

launch["frame-011"] = function(){
		theFrame = $("#frame-011"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		sugar = $(prefix + " .sugar"),
		carbonDioxide = $(prefix + " .carbon-dioxide"),
		hydrogenDioxide = $(prefix + " .hydrogen-dioxide"), 
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	doorOpen.fadeOut(0);
	sugar.fadeOut(0);
	carbonDioxide.fadeOut(0);
	hydrogenDioxide.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/s5-1.mp3");
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 6 && currTime != doneSecond)
		{
			doneSecond = currTime;
			sugar.fadeIn(500);
			timeout[0] = setTimeout(function(){
				carbonDioxide.fadeIn(500);
				timeout[1] = setTimeout(function(){
					hydrogenDioxide.fadeIn(500);
					timeout[2] = setTimeout(function(){
						fadeNavsIn();
					}, 6000);
				}, 1000);
			}, 1000);
		}
	});
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
	});
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
	}
}

launch["frame-012"] = function(){
		theFrame = $("#frame-012"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		griddles = $(prefix + " .griddles"),
		recipeTitle = $(prefix + " .recipe-title"),
		recipe = $(prefix + " .recipe"),
		griddlesResult = $(prefix + " .griddles-result"),
		relativeMassEng = $(prefix + " .relative-mass-eng"),
		relativeMassRus = $(prefix + " .relative-mass-rus"), 
		relativeMassKaz = $(prefix + " .relative-mass-kaz"),
		formulaAnimated = $(prefix + " .formula-animated"),
	
	naclSprite = new Motio(recipe[0], {
		fps: 3,
		frames: 26
	});
	
	naclSprite.on("frame", function(){
		if(this.frame === 10)
		{
			this.pause();
			audio[2].play();
		}
		if(this.frame === 18)
		{
			this.pause();
		}
		if(this.frame === this.frames - 1)
		{
			this.pause();
		}
	});
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 4
	});
	
	formulaSprite = new Motio(formulaAnimated[0], {
		fps: 1,
		frames: 4
	});
	
	formulaSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
		}
	});
	
	doorOpen.fadeOut(0);
	fadeNavsIn();
	griddles.fadeOut(0);
	recipeTitle.fadeOut(0);
	recipe.fadeOut(0);
	griddlesResult.fadeOut(0);
	relativeMassRus.fadeOut(0);
	relativeMassEng.fadeOut(0);
	relativeMassKaz.fadeOut(0);
	formulaAnimated.fadeOut(0);
	
	audio[0] = new Audio("audio/s5-2.mp3");
	audio[1] = new Audio("audio/s5-3.mp3");
	audio[2] = new Audio("audio/s5-4.mp3");
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
		boy.fadeOut(0);
		timeout[0] = setTimeout(function(){
			recipe.fadeIn(1000);
			recipeTitle.fadeIn(1500);
			griddlesResult.fadeIn(1500);
			audio[1].play();
		}, 1000);
	});
	
	audio[1].addEventListener("ended", function(){
		recipe.animate({
			"left": "35%"
		}, 1000);
		recipeTitle.fadeOut(1000);
		griddles.fadeOut(1000);
		griddlesResult.fadeOut(1000);
		naclSprite.play();
	});
	
	audio[2].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 6 && currTime != doneSecond)
		{
			doneSecond = currTime;
			naclSprite.play();
		}
		else if(currTime === 10 && currTime != doneSecond)
		{
			doneSecond = currTime;
			naclSprite.play();
		}
		else if(currTime === 15 && currTime != doneSecond)
		{
			doneSecond = currTime;
			relativeMassRus.fadeIn(0);
			timeout[0] = setTimeout(function(){
				relativeMassEng.fadeIn(0);
			}, 1000);
			timeout[0] = setTimeout(function(){
				relativeMassKaz.fadeIn(0);
			}, 2000);
		}
		else if(currTime === 20 && currTime != doneSecond)
		{
			doneSecond = currTime;
			relativeMassRus.fadeOut(0);
			relativeMassEng.fadeOut(0);
			relativeMassKaz.fadeOut(0);
			recipe.fadeOut(0);
			formulaAnimated.fadeIn(0);
			formulaSprite.play();
		}
	});
	
	audio[2].addEventListener("ended", function(){
		timeout[2] = setTimeout(function(){
			fadeNavsIn();
		}, 3000);
	});
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
		griddles.fadeIn(500);
	}
}

launch["frame-013"] = function(){
		theFrame = $("#frame-013"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		task1 = $(prefix + " .task-1"), 
		task2 = $(prefix + " .task-2"), 
		task3 = $(prefix + " .task-3"),
		everything = $(prefix + " .task"),
		buttons = $(prefix + " .button"),
		abacus = $(prefix + " .abacus"),
		resultLabel = $(prefix + " .result-label"), 
	
	activeQuestion = 2;
	
	audio[0] = new Audio("audio/s5-51.mp3");
	audio[1] = new Audio("audio/s5-53.mp3");
	audio[3] = new Audio("audio/s5-52.mp3");
	
	var boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
		boy.fadeOut(500);
	});
	
	audio[1].addEventListener("ended", function(){
		boySprite.pause();
		boy.fadeOut(500);
	});
	
	fadeNavsIn();
	boy.fadeOut(0);
	everything.fadeOut(0);
	$(prefix + " .task-"+activeQuestion).fadeIn(0);
	
	startButtonListener = function(){
		audio[3].play();
	}
		
	var buttonsListener = function(){
		if($(this).attr("data-correct"))
		{
			activeQuestion --;
			everything.fadeOut(0);
			$(prefix + " .task-"+activeQuestion).fadeIn(0);
			boy.fadeIn(0);
			boySprite.play();
			audio[0].play();
		}
	};
	
	buttons.off("click", buttonsListener);
	buttons.on("click", buttonsListener);
	
	var task1Draggables = $(prefix + " .task-1.ball"),
		task1Draggabillies = [], vegetable, basket;
	
	for (var i = 0; i < task1Draggables.length; i ++)
		task1Draggabillies[i] = new Draggabilly(task1Draggables[i]);
	
	var onStart = function(instance, event, pointer){
		vegetable = $(event.target);
	};
	var onEnd = function(instance, event, pointer){
		vegetable.fadeOut(0);
		
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(vegetable.attr("data-key") === basket.attr("data-key"))
		{
			basket.html(vegetable.html());
			vegetable.remove();
			
			if(!$(prefix + " .task-1.ball").length)
			{
				activeQuestion --;
				everything.fadeOut(0);
				$(prefix + " .task-"+activeQuestion).fadeIn(0);
				if(activeQuestion === 0)
				{
					abacus.css({
						"width": "1%",
						"height": "1%",
						"left": "0%",
						"top": "0%"});
					
					resultLabel.css({
						"opacity": "0",
						"width": "0%",
						"height": "0%"
					});
					
					timeout[0] = setTimeout(function(){
						abacus.addClass("transition-2s");
						resultLabel.addClass("transition-2s");
						resultLabel.css({
							"opacity": "",
							"width": "",
							"height": ""
						});
						timeout[1] = setTimeout(function(){
							abacus.css({
								"width": "",
								"height": "",
								"left": "",
								"top": ""});
							audio[1].play();
							boy.fadeIn(0);
							boySprite.play();
							timeout[2] = setTimeout(function(){
								abacus.css({
									"width": "10%",
									"height": "20%",
									"left": "90%",
									"top": "80%"
								});
								fadeNavsIn();
							},6000);
						}, 1000);
					}, 1000);
				}
			}
		}
		else
		{
			vegetable.fadeIn(0);
			vegetable.css("left", "");
			vegetable.css("top", "");
		}
	};
	
	for (var i = 0; i < task1Draggabillies.length; i ++)
	{
		task1Draggabillies[i].on("dragStart", onStart);
		task1Draggabillies[i].on("dragEnd", onEnd);
	}

}

launch["frame-014"] = function(){
		theFrame = $("#frame-014"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		barrel = $(prefix + " .barrel"), 
		lockOpen = $(prefix + " .lock-open"),
		lockClosed = $(prefix + " .lock-closed"),
		abacus = $(prefix + " .abacus"),
		taskLabel = $(prefix + " .task-label"),
		lockCenter = $(prefix + " .lock-center-sprite"),
		lockNums = $(prefix + " .lock-num"),
		lockNumImage1 = $(prefix + " .lock-num-1"),
		lockNumImage2 = $(prefix + " .lock-num-2"),
		lockNumImage3 = $(prefix + " .lock-num-3"),
		lockNumImage4 = $(prefix + " .lock-num-4"),
		lockButton = $(prefix + " .lock-button"),
		lockNumButtons = $(prefix + " .lock-num-button"),
		lockNumButton1 = $(prefix + " .lock-num-button-1"),
		lockNumButton2 = $(prefix + " .lock-num-button-2"),
		lockNumButton3 = $(prefix + " .lock-num-button-3"),
		lockNumButton4 = $(prefix + " .lock-num-button-4"),
		lockNum1 = 1, 
		lockNum2 = 1, 
		lockNum3 = 1, 
		lockNum4 = 1,
	
	lockNum1Sprite = new Motio(lockNumImage1[0], {
		fps: 1,
		frames: 10
	});
	lockNum2Sprite = new Motio(lockNumImage2[0], {
		fps: 1,
		frames: 10
	});
	lockNum3Sprite = new Motio(lockNumImage3[0], {
		fps: 1,
		frames: 10
	});
	lockNum4Sprite = new Motio(lockNumImage4[0], {
		fps: 1,
		frames: 10
	});
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	barrelSprite = new Motio(barrel[0], {
		fps: 1,
		frames: 3
	});
	
	lockOpenSprite = new Motio(lockOpen[0], {
		fps: 1,
		frames: 2
	});
	
	lockClosedSprite = new Motio(lockClosed[0], {
		fps: 1,
		frames: 2
	});
	
	lockCenterSprite = new Motio(lockCenter[0], {
		fps: 1, 
		frames: 2
	});
	
	audio[0] = new Audio("audio/s6-1.mp3");
	audio[1] = new Audio("audio/water-sound.mp3");
		
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 20 && currTime != doneSecond)
		{
			doneSecond = currTime;
			boySprite.pause();
			lockClosedSprite.to(1, true);
			timeout[0] = setTimeout(function(){
				lockClosedSprite.to(0, true);
				boySprite.play();
			}, 1000);
		}
	});
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
		boy.fadeOut(0);
	});
	
	lockOpen.fadeOut(0);
	lockCenter.fadeOut(0);
	lockNums.fadeOut(0);
	doorOpen.fadeOut(0);
	fadeNavsIn();
	lockNumButtons.fadeOut(0);
	bg2.fadeOut(0);
	
	lockClosedSprite.to(2, true);
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
		timeout[0] = setTimeout(function(){
			barrelSprite.to(1, true);
		}, 10000);
		timeout[1] = setTimeout(function(){
			barrelSprite.to(2, true);
		}, 12000);
		timeout[1] = setTimeout(function(){
			barrelSprite.to(0, true);
		}, 13000);
	};
	
	var lockButtonListener = function(){
		boy.fadeOut(0);
		lockCenter.fadeIn(0);
		lockNums.fadeIn(0);
		lockNumButtons.fadeIn(0);
	};
	
	lockButton.click(lockButtonListener);
	
	var lockNumButtonsListener = function(){
		if($(this).hasClass("lock-num-button-1"))
		{	
			lockNum1 ++;
			if(lockNum1 > 10)
				lockNum1 = 1;
			lockNum4Sprite.to(lockNum1 - 1, true);
		}
		if($(this).hasClass("lock-num-button-2"))
		{	
			lockNum2 ++;
			if(lockNum2 > 10)
				lockNum2 = 1;
			lockNum3Sprite.to(lockNum2 - 1, true);
		}
		if($(this).hasClass("lock-num-button-3"))
		{	
			lockNum3 ++;
			if(lockNum3 > 10)
				lockNum3 = 1;
			lockNum2Sprite.to(lockNum3 - 1, true);
		}
		if($(this).hasClass("lock-num-button-4"))
		{	
			lockNum4 ++;
			if(lockNum4 > 10)
				lockNum4 = 1;
			lockNum1Sprite.to(lockNum4 - 1, true);
		}
		
		if(lockNum1 === 2 && lockNum2 === 10
			&& lockNum3 === 10 && lockNum4 === 10)
			{
				audio[1].play();
				bg1.fadeOut(500);
				bg2.fadeIn(500);
				lockCenter.fadeOut(0);
				lockNums.fadeOut(0);
				lockNumButtons.fadeOut(0);
				lockClosed.fadeOut(0);
				timeout[1] = setTimeout(function(){
					fadeNavsIn();
				}, 3000);
			}	
	}
	lockNumButtons.off("click", lockNumButtonsListener);
	lockNumButtons.on("click", lockNumButtonsListener);	
}

launch["frame-015"] = function(){
		theFrame = $("#frame-015"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boy = $(prefix + " .boy"),
		doorClosed = $(prefix + " .door-closed"),
		doorOpen = $(prefix + " .door-open"),
		jug = $(prefix + " .jug"),
		keepThinking = $(prefix + " .keep-thinking"),
		key = $(prefix + " .key"),
		clayExit = $(prefix + " .clay-exit"), 
	
	boySprite = new Motio(boy[0], {
		fps: 6,
		frames: 2
	});
	
	clayExitSprite = new Motio(clayExit[0], {
		fps: 2,
		frames: 9
	});
	
	clayExitSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			fadeNavsIn();
		}
	});
	
	doorOpen.fadeOut(0);
	fadeNavsIn();
	keepThinking.fadeOut(0);
	clayExit.fadeOut(0);
	
	audio[0] = new Audio("audio/s6-2.mp3");
	audio[3] = new Audio("audio/footsteps.mp3");
	audio[4] = new Audio("audio/door-open.mp3");
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 5 && currTime != doneSecond)
		{
			doneSecond = currTime;
			jug.css("background-position", "100%");
			timeout[2] = setTimeout(function(){
				jug.css("background-position", "");
			},2000);
		}
	});
	
	audio[0].addEventListener("ended", function(){
		boySprite.pause();
		boy.fadeOut(500);
	});
	
	startButtonListener = function(){
		boySprite.play();
		audio[0].play();
	}
	
	var jugListener = function()
	{
		if($(this).attr("data-correct"))
		{
			key.addClass("transition-2s");
			key.css("left", "9%");
			key.css("top", "60%");
			timeout[0] = setTimeout(function(){
				audio[4].play();
				clayExit.fadeIn(0);
				timeout[1] = setTimeout(function(){
					clayExitSprite.play();
					audio[3].play();
				}, 1000);
			}, 3000);
		}
		else
		{
			keepThinking.fadeIn(0);
			timeout[0] = setTimeout(function(){
				keepThinking.fadeOut(0);
			}, 2000);
		}
	}
	
	jug.off("click", jugListener);
	jug.on("click", jugListener);
}

launch["frame-016"] = function(){
		theFrame = $("#frame-016"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		avogadro = $(prefix + " .avogadro"),
		chestOpen = $(prefix + " .chest-open"),
		chestClosed = $(prefix + " .chest-closed"),
		coinAnimated = $(prefix + " .coin-animated"),
		coinInfoAnimated = $(prefix + " .coin-info-animated"),
		hand = $(prefix + " .hand"),
		abacusJug = $(prefix + " .abacus, " + prefix + " .jug"),
		abacusAnimated = $(prefix + " .abacus-animated"),
		abacusCount = 0,
		jug = $(prefix + " .jug"),
	
	abacusSprite = new Motio(abacusAnimated[0], {
		fps: 1,
		frames: 15
	});
	
	handSprite = new Motio(hand[0], {
		fps: 10, 
		frames: 12
	});
	
	avogadroSprite = new Motio(avogadro[0], {
		fps: 6,
		frames: 2
	});
	
	coinInfoSprite = new Motio(coinInfoAnimated[0], {
		fps: 1/2,
		frames: 5
	});
	
	handSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			this.to(0, true);
		}
	});
	
	audio[0] = new Audio("audio/s8-1.mp3");
	audio[6] = new Audio("audio/chest-close.mp3");
	audio[7] = new Audio("audio/coins-sound.mp3");
	audio[8] = new AudioPiece("s8-1", 63, 100);
	audio[9] = new Audio("audio/main-theme.mp3");
	
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 10 && currTime != doneSecond)
		{
			doneSecond = currTime;
			chestOpen.fadeIn(0);
			chestClosed.fadeOut(0);
		}
		if(currTime === 20 && currTime != doneSecond)
		{
			doneSecond = currTime;
			chestOpen.fadeOut(0);
			chestClosed.fadeIn(0);
			chestClosed.html("0,63 МОЛЬ ЗОЛОТА (AU)");
			timeout[0] = setTimeout(function(){
				chestClosed.addClass("transition-2s");
				chestClosed.css("top", "50%");
				chestOpen.css("top", "50%");
				coinAnimated.css("left", "40%");
				coinAnimated.fadeIn(500);
				coinInfoAnimated.fadeIn(0);
				coinInfoSprite.play();
			}, 2000);
		}
		if(currTime === 30 && currTime != doneSecond)
		{
			doneSecond = currTime;
			var chestListener = function(){
				chestClosed.fadeOut(0);
				chestOpen.fadeIn(0);
				audio[6].play();
				timeout[1] = setTimeout(function(){
					chestOpen.fadeOut(0);
					coinAnimated.fadeOut(0);
					coinInfoAnimated.fadeOut(0);
					avogadro.fadeOut(0);
					theFrame.css("background-image", "url(pics/chest-inside.png)");
					theFrame.css("background-size", "100%");
					hand.fadeIn(0);
					theFrame.click(function(){
						audio[7].play();
						handSprite.play();
						abacusAnimated.fadeIn(0);
						abacusJug.fadeOut(0);
						abacusCount ++;
						abacusSprite.to(abacusCount, true);
						if(abacusCount === 10)
						{
							audio[8].play();
							theFrame.css("background-image", "");
							avogadro.fadeIn(0);
							avogadroSprite.play();
							jug.fadeIn(0);
							jug.css({
								"width": "25%",
								"height": "50%",
								"left": "35%", 
								"top": "25%"
							});
							hand.fadeOut(0);
							abacusAnimated.fadeOut(0);
							timeout[1] = setTimeout(function(){
								doneSecond = currTime;
								currAudio.pause();
								avogadroSprite.pause();
								avogadro.addClass("transition-2s");
								avogadro.css("opacity", "0");
								theFrame.css("background-image", "url(pics/s0-bg-2.jpg)");
								theFrame.addClass("transition-2s");
								audio[9].play();
								theFrame.css("background-size", "150%");
								theFrame.css("background-position", "center");
								timeout[0] = setTimeout(function(){
									theFrame.css("opacity", "0");
									soundToZero(audio[9], 7);
								}, 2000);
								timeout[0] = setTimeout(function(){
									fadeNavsIn();
								}, 4000);
							}, 2000);
						}
					});
				}, 2000);
			}
			chestClosed.off("click", chestListener);
			chestClosed.on("click", chestListener);
		}
		if(currTime === 53 && currTime != doneSecond)
		{
			doneSecond = currTime;
			currAudio.pause();
			avogadroSprite.pause();
		}
	});
	
	fadeNavsIn();
	chestOpen.fadeOut(0);
	coinAnimated.fadeOut(0);
	coinInfoAnimated.fadeOut(0);
	hand.fadeOut(0);
	abacusAnimated.fadeOut(0);
	
	startButtonListener = function(){
		avogadroSprite.play();
		audio[0].play();
	}
}

var hideEverythingBut = function(elem)
{
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	

	for (var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);
	
	launch[elem.attr("id")]();
	
	if(elem.attr("id") === "frame-000")
		fadeNavsOut();
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
		buttonSound.play();
	});
};

var main = function()
{
	initMenuButtons();
	//hideEverythingBut($("#pre-load"));
	hideEverythingBut($("#frame-000"));
	//loadImages();
};

$(document).ready(main);